import rc from 'rc'

export type ConfigT = {
	mssql: {
		url: string,
		user: string,
		password: string,
		database: string
	}
}

export function getConfig(name: string): ConfigT {
	const config = rc(name);
	if (!config) {
		throw new Error(`Config by name ${name} not found!`)
	}
	return <ConfigT>config
}