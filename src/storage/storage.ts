import { Request, Response } from 'express';
import pool from "../configs/database"
const mssql = require('mssql');

export async function getRoutes(req: Request, res: Response): Promise<Response | void> {

	try {
		let request = new mssql.Request(pool);
		let result = await request.query(`SELECT ro.route_id id, ro.number_route, ro.name, ro.type, ro.schedule, ro.price, ro.currency 
			FROM routes ro`)

		return res.json(result.recordset);
	} catch (err) {
		console.log(err);
	} finally {
		mssql.close();
	}
}

export async function getRoutesByStopId(req: Request, res: Response): Promise<Response | void> {

	try {
		let request = new mssql.Request(pool);
		let result = await request
			.input('stopId', mssql.Int, req.params.stopId)
			.query(`SELECT * FROM stops s WHERE s.stop_id = @stopId`);
		let routeIds: any = [];
		for (let i = 0; i < result.recordset.length; i++) {
			routeIds.push({ routeId: result.recordset[i].route_id });
		}
		return res.json(routeIds);
	} catch (err) {
		console.log(err);
	} finally {
		mssql.close();
	}
}

export async function getRouteById(req: Request, res: Response): Promise<Response | void> {

	try {
		let request = new mssql.Request(pool);
		let result = await request
			.input('routeId', mssql.Int, req.params.routeId)
			.query(`SELECT r.route_id, r.name, r.interval, r.currency, r.number_route, r.price, r.schedule, rs.begin_x, rs.begin_y, rs.end_x, rs.end_y, rs.backward
FROM routes r INNER join routes_schema rs on r.route_id = rs.route_id
WHERE r.route_id = @routeId`);
		return res.json(result.recordset);
	} catch (err) {
		console.log(err);
	} finally {
		mssql.close();
	}
}

export async function getVehiclesDb(): Promise<any> {

	try {
		let request = new mssql.Request(pool);
		let result = await request
			.query(`SELECT ag.Mar_num routeNumber, ag.Latitude latitude, ag.Longitude longitude, ag.TranspID type, ag.Num_Pe vehicleId, t1.maxTime FROM Aggr ag INNER JOIN 
			(SELECT
			 Unit_no,  MAX(GPS_datetime) maxTime
			FROM Aggr
			GROUP BY Unit_no
      		HAVING DATEADD(MINUTE, -5, CURRENT_TIMESTAMP) < MAX(GPS_datetime)) t1 on ag.Unit_no = t1.Unit_no and t1.maxTime = ag.GPS_datetime`);

		return result.recordset;
	} catch (err) {
		console.log(err);
	} finally {
		mssql.close();
	}
}