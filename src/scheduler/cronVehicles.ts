import cron from "node-cron";
import pool from "../configs/database"
const mssql = require('mssql');

const schedule = cron.schedule("0 0 */1 * * *", async () => {
	await deleteOldRows()
});

async function deleteOldRows() {

	try {
		let request = new mssql.Request(pool);
		await request.query(`DELETE
FROM smcity_.smart.Aggr
WHERE GPS_datetime <= DATEADD(MINUTE, -60, CURRENT_TIMESTAMP)`);

	} catch (err) {
		console.log(err);
	} finally {
		mssql.close();
	}
}

export function initScheduler(): cron.ScheduledTask {
	return schedule;
}