import { Router } from 'express';
import { getRoutes, getRoutesByStopId, getRouteById, getVehiclesDb } from '../storage/storage';
const cors = require("cors");
const router = Router();
const server = require("http").Server(router);
const io = require("socket.io")(server);

router.use(
	cors({
		origin: true,
	}),
);

router.route('/routes')
	.get(getRoutes);

router.route('/stops/:stopId')
	.get(getRoutesByStopId);

router.route('/routes/:routeId')
	.get(getRouteById);

io.set("origins", "*:*");

io.on("connection", function (socket: { emit: (arg0: string, arg1: any) => void; }) {
	socket.emit("vehicles_update", vehiclesTemp);
});

let vehiclesTemp: any = undefined;

async function getVehicles() {
	const data = await getVehiclesDb();

	if (data) {
		const vehicles = data.map((vehicle: { routeNumber: any; direction: any; latitude: any; longitude: any; type: any; vehicleID: any; }) => {
			return {
				routeNumber: vehicle.routeNumber,
				direction: vehicle.direction,
				latitude: vehicle.latitude,
				longitude: vehicle.longitude,
				type: vehicle.type,
				vehicleID: vehicle.vehicleID,
			};
		});
		vehiclesTemp = vehicles;
		io.emit("vehicles_update", vehicles);
	}
}

setInterval(() => getVehicles(), 5000);

server.listen(3001);

export default router;